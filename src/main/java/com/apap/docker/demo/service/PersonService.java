package com.apap.docker.demo.service;

import com.apap.docker.demo.model.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PersonService {

    private final List<Person> persons = new ArrayList<>();

    // Method untuk menambahkan orang baru
    public Person addPerson(Person person) {
        persons.add(person);
        return person;
    }

    // Method untuk mendapatkan semua orang
    public List<Person> getAllPersons() {
        return new ArrayList<>(persons); // Mengembalikan salinan daftar untuk menghindari modifikasi eksternal
    }

    // Method untuk mendapatkan orang berdasarkan ID
    public Optional<Person> getPersonById(Long id) {
        return persons.stream()
                .filter(person -> person.getId().equals(id))
                .findFirst();
    }

    // Method untuk memperbarui data orang
    public Person updatePerson(Long id, Person personDetails) {
        Optional<Person> optionalPerson = getPersonById(id);

        if(optionalPerson.isPresent()) {
            Person existingPerson = optionalPerson.get();
            existingPerson.setName(personDetails.getName());
            existingPerson.setAge(personDetails.getAge());
            // tambahkan set method lainnya jika diperlukan

            return existingPerson;
        } else {
            throw new RuntimeException("Person not found with id " + id);
        }
    }

    // Method untuk menghapus orang berdasarkan ID
    public void deletePerson(Long id) {
        persons.removeIf(person -> person.getId().equals(id));
    }


}
