# Gunakan base image Java 17. Anda dapat memilih versi lain jika diperlukan.
FROM openjdk:17-jdk-slim

# Copy file JAR ke dalam image
COPY target/*.jar app.jar

# Expose port yang digunakan Spring Boot (default: 8080)
EXPOSE 8080

# Command untuk menjalankan aplikasi Spring Boot
ENTRYPOINT ["java","-jar","/app.jar"]
